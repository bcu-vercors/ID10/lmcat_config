
from bliss.setup_globals import *

load_script("lmcat_camera_swir.py")

print("")
print("Welcome to your new 'lmcat_camera_swir' BLISS session !! ")
print("")
print("You can now customize your 'lmcat_camera_swir' session by changing files:")
print("   * /lmcat_camera_swir_setup.py ")
print("   * /lmcat_camera_swir.yml ")
print("   * /scripts/lmcat_camera_swir.py ")
print("")