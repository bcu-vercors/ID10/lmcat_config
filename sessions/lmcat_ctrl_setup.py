
from bliss.setup_globals import *

load_script("lmcat_nanodac.py")
load_script("lmcat_init.py")
load_script("lmcat_shutdown.py")
load_script("lmcat_flowctrl.py")
load_script("lmcat_tempctrl.py")
load_script("lmcat_frontpanel.py")
load_script("lmcat_pumpdown.py")

print("")
print("Welcome to your new 'lmcat_nanodac' BLISS session !! ")
print("")
print("You can now customize your 'lmcat_nanodac' session by changing files:")
print("   * /lmcat_nanodac_setup.py ")
print("   * /lmcat_nanodac.yml ")
print("   * /scripts/lmcat_nanodac.py ")
print("")



def P(value):
    
    lmcat.flow.lm_Pressure.setpoint=value
    
    
def Ar(value):
    
    lmcat.flow.lm_Ar.setpoint=value
    
def H2(value):
    
    lmcat.flow.lm_H2.setpoint=value
    

def CH4(value):
    
    lmcat.flow.lm_CH4.setpoint=value
    
def ArAux(value):
    
    lmcat.flow.lm_ArAux.setpoint=value
    
def RamanPort(value):
    
    lmcat.flow.lm_RamanPort.setpoint=value
    
v_ArAux=lmcat.frontpanel.V1
v_ArAux_to_pump=lmcat.frontpanel.V2
v_CH4=lmcat.frontpanel.V3
v_CH4_to_pump=lmcat.frontpanel.V4
v_H2=lmcat.frontpanel.V5
v_H2_to_pump=lmcat.frontpanel.V6
v_Ar=lmcat.frontpanel.V7
v_rector_in=lmcat.frontpanel.V8
v_reactor_out=lmcat.frontpanel.V10








