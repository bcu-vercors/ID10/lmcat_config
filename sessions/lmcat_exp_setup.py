
from bliss.setup_globals import *

load_script("lmcat_exp.py")
load_script("lmcat_init.py")
load_script("lmcat_shutdown.py")
load_script("lmcat_frontpanel.py")
load_script("lmcat_flowctrl.py")



print("")
print("Welcome to your new 'lmcat_exp' BLISS session !! ")
print("")
print("You can now customize your 'lmcat_exp' session by changing files:")
print("   * /lmcat_exp_setup.py ")
print("   * /lmcat_exp.yml ")
print("   * /scripts/lmcat_exp.py ")
print("")

def lmcat_enable_log():
    import logging
    logging.getLogger("bliss.ctrl.bronkhorst").setLevel(logging.DEBUG)

lmcat_enable_log()