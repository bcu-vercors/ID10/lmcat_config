
from bliss.setup_globals import *

load_script("hpcat_exp.py")

print("")
print("Welcome to your new 'hpcat_exp' BLISS session !! ")
print("")
print("You can now customize your 'hpcat_exp' session by changing files:")
print("   * /hpcat_exp_setup.py ")
print("   * /hpcat_exp.yml ")
print("   * /scripts/hpcat_exp.py ")
print("")