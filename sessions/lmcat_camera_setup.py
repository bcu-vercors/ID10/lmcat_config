
from bliss.setup_globals import *

load_script("lmcat_camera.py")

print("")
print("Welcome to your new 'lmcat_camera' BLISS session !! ")
print("")
print("You can now customize your 'lmcat_camera' session by changing files:")
print("   * /lmcat_camera_setup.py ")
print("   * /lmcat_camera.yml ")
print("   * /scripts/lmcat_camera.py ")
print("")