def lmcat_shutdown():
  # Step 1
  while True:
    step1 = input("Confirm reactor is cooled down (press Enter to continue)").lower()
    if step1 == "":
      break

  print("Closing V1")
  lmcat.flow.lm_ArAux.setpoint=0
  lmcat.frontpanel.V1.close()
  time.sleep(0.5)

  print("Closing V3")
  lmcat.flow.lm_CH4.setpoint=0
  lmcat.frontpanel.V3.close()
  time.sleep(0.5)

  print("Setting H2 setpoint to 0")
  lmcat.flow.lm_H2.setpoint=0
  time.sleep(0.5)

  print("Closing V5")
  lmcat.frontpanel.V5.close()
  time.sleep(0.5)

  print("Setting reactor pressure to 1 bar")
  lmcat.flow.lm_Pressure.setpoint=1
  lmcat.flow.lm_Ar.setpoint=300
  #waiting for pressure to go down 
  reactor_pressure= float(lmcat.flow.lm_Pressure.measure)
  while reactor_pressure <0.97: 
    print("\rWaiting for pressure to reach setpoint. Current pressure is: {:.3f} bar".format(lmcat.flow.lm_Pressure.measure), end='', flush=True)
    time.sleep(1) #Announcing current pressure every 5 seconds
    reactor_pressure= float(lmcat.flow.lm_Pressure.measure)
    
  print()
  print("Setting Ar setpoint to 0")
  lmcat.flow.lm_Ar.setpoint=0
  time.sleep(0.5)
  
  print("Closing V8")
  lmcat.frontpanel.V8.close()
  time.sleep(0.5)

  print("Closing V10")
  lmcat.frontpanel.V10.close()
  time.sleep(0.5)

  print("Setting Raman Port flow to 0")
  lmcat.flow.lm_RamanPort.setpoint=0
  time.sleep(0.5)

  print("Closing V7")
  lmcat.frontpanel.V7.close()
  time.sleep(0.5)

  print("Shut down procedure complete.\nBe sure to close gas bottles, turn off the water cooler and pump, and close air valve.")

