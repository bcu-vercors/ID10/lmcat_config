def lmcat_pumpdown():
  # Step 1
  while True:
    step1 = input("Confirm reactor is cooled down (press Enter to continue)").lower()
    if step1 == "":
      break

  lmcat.frontpanel.V10.open()
  print("Closing V1")
  lmcat.flow.lm_ArAux.setpoint=0
  lmcat.frontpanel.V1.close()
  time.sleep(0.5)

  print("Closing V3")
  lmcat.flow.lm_CH4.setpoint=0
  lmcat.frontpanel.V3.close()
  time.sleep(0.5)

  print("Setting H2 setpoint to 0")
  lmcat.flow.lm_H2.setpoint=0
  time.sleep(0.5)

  print("Closing V5")
  lmcat.frontpanel.V5.close()
  time.sleep(0.5)
  
  print("Setting Ar setpoint to 0")
  lmcat.flow.lm_Ar.setpoint=0
  time.sleep(0.5)
  
  print("Setting reactor pressure to 0 bar")
  lmcat.flow.lm_Pressure.setpoint=0
  
  
  #waiting for pressure to go down 
  reactor_pressure= float(lmcat.flow.lm_Pressure.measure)
  lmcat.flow.lm_Pressure.setpoint=0
  reactor_pressure= float(lmcat.flow.lm_Pressure.measure)
  while reactor_pressure > 0.001:
    print("\rWaiting for pressure to reach setpoint. Current pressure is: {:.3f} bar".format(lmcat.flow.lm_Pressure.measure), end='', flush=True)
    time.sleep(1) #Announcing current pressure every 1s
    reactor_pressure= float(lmcat.flow.lm_Pressure.measure)
  print()
  print("Setting Raman Port flow to 0")
    
  lmcat.flow.lm_RamanPort.setpoint=0
  time.sleep(0.5)
  
  print("Closing all valves")
  lmcat.frontpanel.close_all()
  time.sleep(0.5)





  print("pump down procedure complete.\nBe sure to close gas bottles, turn off the water cooler and pump, and close air valve.")

