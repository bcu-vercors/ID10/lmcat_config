class Frontpanel:
    def __init__(self, lmcat):
        self.lmcat = lmcat
        self.V1 = self.Valve(self, 0, 'V1')
        self.V2 = self.Valve(self, 1, 'V2')
        self.V3 = self.Valve(self, 2, 'V3')
        self.V4 = self.Valve(self, 3, 'V4')
        self.V5 = self.Valve(self, 4, 'V5')
        self.V6 = self.Valve(self, 5, 'V6')
        self.V7 = self.Valve(self, 6, 'V7')
        self.V8 = self.Valve(self, 7, 'V8')
        self.V9 = self.Valve(self, 8, 'V9')
        self.V10 = self.Valve(self, 9, 'V10')
        self.pump = self.Valve(self, 12, 'pump')
        
    # Override __getattr__ to return frontpanel value when instance is referenced directly
    def __getattr__(self, attr):
        if attr == '__repr__':
            return self.lmcat.frontpanel.__repr__  # Delegate repr to lmcat.frontpanel's repr
        return getattr(self.lmcat.frontpanel, attr)
    
    def __repr__(self):
        return repr(self.lmcat.frontpanel)

    class Valve:
        def __init__(self, parent, index, name):
            self.parent = parent
            self.index = index
            self.name = name

        @property
        def status(self):
            """Get the valve status"""
            return self.parent.lmcat.frontpanel.states[self.index]

        def open(self):
            if self.index == 0 and self.parent.lmcat.frontpanel.states[1] == 1:
                while True:
                    notice = input("V2 is open, close V2 first? Y/N: ").lower()
                    if notice == "y":
                        self.parent.lmcat.frontpanel.V2.close()
                        time.sleep(0.5)
                        self.parent.lmcat.frontpanel.V1.open()
                        break
                    elif notice == "n":
                        raise ValueError("Cannot open V1 while V2 is open")
            
            elif self.index == 1 and self.parent.lmcat.frontpanel.states[0] == 1:
                while True:
                    notice = input("V1 is open, close V1 first? Y/N: ").lower()
                    if notice == "y":
                        self.parent.lmcat.frontpanel.V1.close()
                        time.sleep(0.5)
                        self.parent.lmcat.frontpanel.V2.open()
                        break
                    elif notice == "n":
                        raise ValueError("Cannot open V2 while V1 is open")
            
            elif self.index == 2 and self.parent.lmcat.frontpanel.states[3] == 1:
                while True:
                    notice = input("V4 is open, close V4 first? Y/N: ").lower()
                    if notice == "y":
                        self.parent.lmcat.frontpanel.V4.close()
                        time.sleep(0.5)
                        self.parent.lmcat.frontpanel.V3.open()
                        break
                    elif notice == "n":
                        raise ValueError("Cannot open V3 while V4 is open")

            elif self.index == 3 and self.parent.lmcat.frontpanel.states[2] == 1:
                while True:
                    notice = input("V3 is open, close V3 first? Y/N: ").lower()
                    if notice == "y":
                        self.parent.lmcat.frontpanel.V3.close()
                        time.sleep(0.5)
                        self.parent.lmcat.frontpanel.V4.open()
                        break
                    elif notice == "n":
                        raise ValueError("Cannot open V4 while V3 is open")
            
            elif self.index == 5 and self.parent.lmcat.frontpanel.states[4] == 1 or self.index == 4 and self.parent.lmcat.frontpanel.states[5] == 1:
                raise ValueError("Cannot open V6 and V5 simultaniously")
            
            else:
                getattr(self.parent.lmcat.frontpanel, self.name).open()
        def close(self):
            """Close valve"""
            if self.index in {4, 6, 7, 8, 9, 12}:
                while True:
                    notice = input(f"You are about to close {self.name}. Press Enter to proceed: ").lower()
                    if notice == "":
                        getattr(self.parent.lmcat.frontpanel, self.name).close()
                        break
                        
            else:      
                getattr(self.parent.lmcat.frontpanel, self.name).close()




FP = Frontpanel(lmcat)


