class Tempctrl:
    def __init__(self, nanodac_heater, nanodac_BN_cell):
        self.nanodac_heater = nanodac_heater
        self.cell = nanodac_BN_cell

    @property
    def Cell(self):
        """Get the AB cell T"""
        return self.cell.setpoint, self.cell.ramprate

    def Reactor(self):
        """Get the current Reactor temperature."""
        return self.nanodac_heater.setpoint , self.nanodac_heater.ramprate

    def AB(self, val, ramp=None):
        """Set AB cell temperature and ramp rate."""
        if ramp:
            self.cell.ramprate = ramp
            self.cell.setpoint = val
        else:
            self.cell.ramprate = 1
            self.cell.setpoint = val
            print("Ramping cell w/ ramprate of 1")
        

    def Cu(self, val, ramp=None):
        """Set reactor temperature and ramp rate."""
        if ramp:
            self.nanodac_heater.ramprate = ramp
            self.nanodac_heater.setpoint = val
        else:
            self.nanodac_heater.ramprate = 0.5
            self.nanodac_heater.setpoint = val
            print("Ramping reactor w/ ramprate of 0.5")
        
        
Temp = Tempctrl(nanodac_heater,nanodac_BN_cell)