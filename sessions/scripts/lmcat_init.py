
import time #remove if already imported

def lmcat_init():

    print('Initiating verification process...')

    # Step 1
    while True:
        step1 = input("Confirm water chiller is on (press Enter to continue)").lower()
        if step1 == "":
            break

    # Step 2
    while True:
        step2 = input("Confirm pump is on (press Enter to continue)").lower()
        if step2 == "":
            break

    # Step 3
    while True:
        step3 = input("Confirm gas bottles are open (press Enter to continue): ").lower()
        if step3 == "":
            break
        

    # Step 4
    while True:
        step4 = input("Confirm air valve is open (press Enter to continue): ").lower()
        if step4 == "":
            break
        
    
    print("Verification complete. Beginning experiment.")
    print("Opening valve V10")
    lmcat.frontpanel.V10.open()
    print("Pumping down the reactor to 0 bar")
    lmcat.flow.lm_Pressure.setpoint=0
    
    #waiting for pressure to go down 
    reactor_pressure= float(lmcat.flow.lm_Pressure.measure)
    while reactor_pressure > 0.001: 
      print("\rWaiting for pressure to reach setpoint. Current pressure is: {:.3f} bar".format(lmcat.flow.lm_Pressure.measure), end='', flush=True)
      time.sleep(1) #Announcing current pressure every 1s
      reactor_pressure= float(lmcat.flow.lm_Pressure.measure)

    print()
    print("Opening RamanPort (N1) flowmeter to 1500 sccm")
    lmcat.flow.lm_RamanPort.setpoint=1500

    print("Opening valve V8")
    lmcat.frontpanel.V8.open()
    time.sleep(0.5)
    print("Opening valve V7")
    lmcat.frontpanel.V7.open()
    time.sleep(0.5)
    print("Opening valve V5")
    lmcat.frontpanel.V5.open()
    time.sleep(0.5)
    print("Opening valve V3")
    lmcat.frontpanel.V3.open()
    time.sleep(0.5)
    print("Opening valve V1")
    lmcat.frontpanel.V1.open()
    
    #waiting for pressure to go down again
    lmcat.flow.lm_Pressure.setpoint=0
    reactor_pressure= float(lmcat.flow.lm_Pressure.measure)
    while reactor_pressure > 0.005:
      print("\rWaiting for pressure to reach setpoint. Current pressure is: {:.3f} bar".format(lmcat.flow.lm_Pressure.measure), end='', flush=True)
      time.sleep(1) #Announcing current pressure every 1s
      reactor_pressure= float(lmcat.flow.lm_Pressure.measure)

    print()
    print("Closing V3")
    lmcat.frontpanel.V3.close()
    print("Closing V1")
    lmcat.frontpanel.V1.close()
    
    print("Pumping the reactor to 0.2 bar")
    lmcat.flow.lm_Pressure.setpoint=0.2
    
    print("Gradually increasing Ar flow to 200 sccm")
    lmcat.flow.lm_Ar.setpoint=0.5
    time.sleep(1)
    lmcat.flow.lm_Ar.setpoint=1
    time.sleep(1)
    lmcat.flow.lm_Ar.setpoint=5
    time.sleep(1)
    lmcat.flow.lm_Ar.setpoint=200

    print("Gradually increasing H2 flow to 20 sccm")
    lmcat.flow.lm_H2.setpoint=0.5
    time.sleep(1)
    lmcat.flow.lm_H2.setpoint=1
    time.sleep(1)
    lmcat.flow.lm_H2.setpoint=5
    time.sleep(1)
    lmcat.flow.lm_H2.setpoint=20

    
    print("Start up procedure complete, you may now heat up the reactor")



#if __name__ == '__main__':
#    lmcat_init()
