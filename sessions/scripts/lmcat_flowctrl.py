
import sys

class Flowctrl:
    
    def __init__(self, lmcat, nanodac_heater):
        self.lmcat = lmcat
        self.nanodac = nanodac_heater
        
    # Override __getattr__ to return lmcat value when instance is referenced directly
    def __getattr__(self, attr):
        if attr == '__repr__':
            return self.lmcat.__repr__  # Delegate repr to lmcat's repr
        return getattr(self.lmcat, attr)
    
    def __repr__(self):
        return repr(self.lmcat)
        
    @property
    def Ar(self):
        """Get the current Argon (Ar) flow rate."""
        return self.lmcat.flow.lm_Ar.setpoint

    @Ar.setter
    def Ar(self, val):
        """Set the Argon (Ar) flow rate with safety checks."""
        if self.nanodac.read() > 5 and val < 100:
            raise ValueError("Cannot set Ar flow to less than 100 sccm, at high T")
        self.lmcat.flow.lm_Ar.setpoint = val

    @property
    def H2(self):
        """Get the current Hydrogen (H2) flow rate."""
        return self.lmcat.flow.lm_H2.setpoint

    @H2.setter
    def H2(self, val):
        """Set the Hydrogen (H2) flow rate with safety checks."""
        if self.nanodac.read() > 5 and val < 7.5:
            raise ValueError("Cannot set H2 flow to less than 8 sccm, at high T")
        self.lmcat.flow.lm_H2.setpoint = val


    @property
    def CH4(self):
        """Get the current Hydrogen (H2) flow rate."""
        return self.lmcat.flow.lm_CH4.setpoint

    @CH4.setter
    def CH4(self, val):
        """Set the Hydrogen (H2) flow rate with safety checks."""
        self.lmcat.flow.lm_CH4.setpoint = val
    @property
    def methane(self):
        """Get the current CH4 flow rate."""
        return self.lmcat.flow.lm_CH4.setpoint
    def CH4_ramp(self, val, ramp=None):
        """Set the CH4 flow rate with optional ramp."""
        max_bar_length = 30  # Maximum length of the loading bar
        if ramp:
            i = self.lmcat.flow.lm_CH4.setpoint
            steps = int((val - i) * 60 / ramp)
            step_size = (val - i) / steps
            print("Ramping CH4 flow rate:")
            for step in range(steps):
                i += step_size
                self.lmcat.flow.lm_CH4.setpoint = round(i, 2)
                time.sleep(1)
                # Loading bar visualization
                bar_length = min(max_bar_length, steps)
                filled_length = int(bar_length * (step + 1) / steps)
                bar = '=' * filled_length + ' ' * (bar_length - filled_length)
                sys.stdout.write(f"\r[{bar}] {round(i, 2)} sccm")
                sys.stdout.flush()
            self.lmcat.flow.lm_CH4.setpoint = val
            print("\nCH4 flow reached setpoint")
        else:
            self.lmcat.flow.lm_CH4.setpoint = val
            print(f"CH4 flow set to {val} sccm without ramping")
    @property
    def Aux(self):
        """Get the current Aux flow rate."""
        return self.lmcat.flow.lm_ArAux.setpoint

    @Aux.setter
    def Aux(self, val):
        """Set the current Aux flow rate."""
        self.lmcat.flow.lm_ArAux.setpoint = val
    def Aux_ramp(self, val, ramp=None):
        """Set the Aux flow rate with optional ramp."""
        max_bar_length = 30  # Maximum length of the loading bar
        if ramp:
            i = self.lmcat.flow.lm_ArAux.setpoint
            steps = int((val - i) * 60 / ramp)
            step_size = (val - i) / steps
            print("Ramping Aux flow rate:")
            for step in range(steps):
                i += step_size
                self.lmcat.flow.lm_ArAux.setpoint = round(i, 2)
                time.sleep(1)
                # Loading bar visualization
                bar_length = min(max_bar_length, steps)
                filled_length = int(bar_length * (step + 1) / steps)
                bar = '=' * filled_length + ' ' * (bar_length - filled_length)
                sys.stdout.write(f"\r[{bar}] {round(i, 2)} sccm")
                sys.stdout.flush()
            self.lmcat.flow.lm_ArAux.setpoint = val
            print("\nAux flow reached setpoint")
        else:
            self.lmcat.flow.lm_ArAux.setpoint = val
            print(f"Aux flow set to {val} sccm without ramping")

    @property
    def RamanPort(self):
        """Get the current Ramanport flow rate."""
        return self.lmcat.flow.lm_RamanPort.setpoint

    @RamanPort.setter
    def RamanPort(self, val):
        """Set the current Ramanport flow rate."""
        self.lmcat.flow.lm_RamanPort.setpoint = val

    @property
    def Pressure(self):
        """Get the current Ramanport flow rate."""
        return self.lmcat.flow.Pressure.setpoint

    @Pressure.setter
    def Pressure(self, val):
        """Set the current Ramanport flow rate."""
        self.lmcat.flow.lm_Pressure.setpoint = val



Flow = Flowctrl(lmcat,nanodac_heater)

